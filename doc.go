// Package sendmail provides a simple interface to compose emails and to mail them
// efficiently.
//
package sendmail
